﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Window
    {
        public Register()
        {
            InitializeComponent();
        }

        private void Submit(object sender, RoutedEventArgs e)
        {
            var jsonMsg = new
            {
                username = username.Text,
                password = password.Text,
                email = email.Text,
                address = address.Text,
                phone = phone.Text,
                birthDate = birthdate.Text
            };

            SignupResponse result = (SignupResponse)MainWindow.commiunicator.handleMsg(jsonMsg, OP_CODES.SIGNUP);      
            if(Convert.ToBoolean(result.status))
            {
                MessageBox.Show("You registered successfully!");
                var newForm = new MainWindow(); //create your new form.
                newForm.Show(); //show the new form.
                Dispatcher.Invoke(() =>
                {
                    // Code causing the exception or requires UI thread access
                    this.Close(); //only if you want to close the current form.
                });
            }
            else
            {
                MessageBox.Show("Your signup has failed. please try again");
            }

        }

        private void GoBack(object sender, RoutedEventArgs e)
        {
            var newForm = new MainWindow(); //create your new form.
            newForm.Show(); //show the new form.
            Dispatcher.Invoke(() =>
            {
                // Code causing the exception or requires UI thread access
                this.Close(); //only if you want to close the current form.
            });
        }
    }
}
