﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
            title.Text = "Hi, " + MainWindow.user.getName();
        }

        private void CreateRoom(object sender, RoutedEventArgs e)
        {
            var newForm = new CreateRoomPage(); //create your new form.
            newForm.Show(); //show the new form.
            Dispatcher.Invoke(() =>
            {
                // Code causing the exception or requires UI thread access
                this.Close(); //only if you want to close the current form.
            });
        }

        private void JoinRoom(object sender, RoutedEventArgs e)
        {
            var newForm = new JoinRoomPage(); //create your new form.
            newForm.Show(); //show the new form.
            Dispatcher.Invoke(() =>
            {
                // Code causing the exception or requires UI thread access
                this.Close(); //only if you want to close the current form.
            });
        }

        private void Logout(object sender, RoutedEventArgs e)
        {

            var newForm = new MainWindow(); //create your new form.
            newForm.Show(); //show the new form.
            Dispatcher.Invoke(() =>
            {
                // Code causing the exception or requires UI thread access
                this.Close(); //only if you want to close the current form.
            });
        }

        private void Statistics(object sender, RoutedEventArgs e)
        {
            var newForm = new StatisticsPage(); //create your new form.
            newForm.Show(); //show the new form.
            Dispatcher.Invoke(() =>
            {
                // Code causing the exception or requires UI thread access
                this.Close(); //only if you want to close the current form.
            });
        }
    }
}
