﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for StatisticsPage.xaml
    /// </summary>
    public partial class StatisticsPage : Window
    {
        public StatisticsPage()
        {
            InitializeComponent();
            var jsonMsg = new {};
            GetStatisticsResponse response = (GetStatisticsResponse)MainWindow.commiunicator.handleMsg(jsonMsg, OP_CODES.HIGH_SCORE);
            
            string finalData = "";
            for(int i = 0; i < response.result.Length; i++)
            {
                finalData += response.result[i] += "\r\n";
            }

            Data.Text = finalData;
        }

        private void GoBack(object sender, RoutedEventArgs e)
        {
            var newForm = new Menu(); //create your new form.
            newForm.Show(); //show the new form.
            Dispatcher.Invoke(() =>
            {
                // Code causing the exception or requires UI thread access
                this.Close(); //only if you want to close the current form.
            });
        }
    }
}
