﻿using System;
using System.Windows;


namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Commiunicator commiunicator;
        public static SettingsManager settingsManager;
        public static User user;
        public MainWindow()
        {
            InitializeComponent();
            commiunicator = new Commiunicator();
            settingsManager = new SettingsManager();
            user = new User("");

            try
            {
                commiunicator.connect();
            } catch
            {
                MessageBox.Show("Cant connect to the server on:\r\n"+
                    settingsManager.getHost() + ':' + settingsManager.getPort());

                this.Close();
            }
        }

        /*
         * A function that runs when the login button is clicked.
         * it creates a json msg, gets a response from the server and checks if the login is ok
         * if so, it moves you to the menu's screen
         */
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var jsonMsg = new
            {
                username = username.Text,
                password = password.Text
            };

            LoginResponse result = (LoginResponse)MainWindow.commiunicator.handleMsg(jsonMsg, OP_CODES.LOGIN);

            if(Convert.ToBoolean(result.status))
            {
                MainWindow.user.setName(username.Text);
                MessageBox.Show("You logged in!");
                //moving to the menu
                var newForm = new Menu(); //create your new form.
                newForm.Show(); //show the new form.
                Dispatcher.Invoke(() =>
                {
                    // Code causing the exception or requires UI thread access
                    this.Close(); //only if you want to close the current form.
                });
            }
            else
            {
                MessageBox.Show("Wrong username or password, try again");
            }

            
        }

        /*
         * Go to the Register screen
         */
        private void Register(object sender, RoutedEventArgs e)
        {
            var newForm = new Register(); //create your new form.
            newForm.Show(); //show the new form.
            Dispatcher.Invoke(() =>
            {
                // Code causing the exception or requires UI thread access
                this.Close(); //only if you want to close the current form.
            });
        }
    }
}
