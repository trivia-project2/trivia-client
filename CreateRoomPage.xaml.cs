﻿using System;
using System.Windows;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for CreateRoomPage.xaml
    /// </summary>
    public partial class CreateRoomPage : Window
    {
        public CreateRoomPage()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try {
                string room_name = roomName.Text;
                int max_users = Convert.ToInt32(maxUsers.Text);
                int question_count = Convert.ToInt32(questionCount.Text);
                int answer_timeout = Convert.ToInt32(answerTimeout.Text);

                var jsonMsg = new {
                    roomName = room_name,
                    maxUsers = max_users,
                    questionCount = question_count,
                    answerTimeout = answer_timeout
                };

                CreateRoomResponse result = (CreateRoomResponse)MainWindow.commiunicator.handleMsg(jsonMsg, OP_CODES.CREAT_ROOM);
                if(Convert.ToBoolean(result.status))
                {
                    MainWindow.user.setAdmin(true);
                    MessageBox.Show("You created the room successfully");
                    var newForm = new WatingRoom(room_name); //create your new form.
                    newForm.Show(); //show the new form.
                    Dispatcher.Invoke(() =>
                    {
                        // Code causing the exception or requires UI thread access
                        this.Close(); //only if you want to close the current form.
                    });
                }
                else
                {
                    MessageBox.Show("The room has failed!");
                }

            } catch(Exception)
            {
                MessageBox.Show("Please insert numbers where you should");
            }
            
            
        }

        private void ToMenu(object sender, RoutedEventArgs e)
        {
            var newForm = new Menu(); //create your new form.
            newForm.Show(); //show the new form.
            Dispatcher.Invoke(() =>
            {
                // Code causing the exception or requires UI thread access
                this.Close(); //only if you want to close the current form.
            });
        }
    }
}
