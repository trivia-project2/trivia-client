﻿using System.Collections.Generic;
using System.Net.Sockets;
using Newtonsoft.Json;


namespace TriviaClient
{
    public class Commiunicator
    {
        private Socket sock;
        public Commiunicator()
        {
            
        }

        /*
         * input: none
         * output: none
         * A method that connects to the socket according to the ip and port in the .ini file
         */
        public void connect()
        {
            sock = new Socket(SocketType.Stream, ProtocolType.Tcp);
            sock.Connect(MainWindow.settingsManager.getHost(), MainWindow.settingsManager.getPort());
        }

        /*
         * input: json object, and an opcode
         * output: the response of the server
         * A method that turns the op code and the json object into a message, sends it, and recives an answer
         */
        public Response handleMsg(object jsonObj, OP_CODES opCode)
        {
            sendMsg(opCode, JsonConvert.SerializeObject(jsonObj));
            string jsonStr = recvJsonStr();

            //TO-DO: turn to a template function
            switch(opCode)
            {
                case OP_CODES.LOGIN:
                    LoginResponse loginResult = JsonConvert.DeserializeObject<LoginResponse>(jsonStr);
                    return loginResult;
                case OP_CODES.SIGNUP:
                    SignupResponse signupResult = JsonConvert.DeserializeObject<SignupResponse>(jsonStr);
                    return signupResult;
                case OP_CODES.CREAT_ROOM:
                    CreateRoomResponse createRoomResult = JsonConvert.DeserializeObject<CreateRoomResponse>(jsonStr);
                    return createRoomResult;
                case OP_CODES.GET_ROOMS:
                    GetRoomResponse getRoomResponse = JsonConvert.DeserializeObject<GetRoomResponse>(jsonStr);
                    return getRoomResponse;
                case OP_CODES.GET_ROOM_STATE:
                    GetRoomStateResponse getRoomStateResponse = JsonConvert.DeserializeObject<GetRoomStateResponse>(jsonStr);
                    return getRoomStateResponse;
                case OP_CODES.JOIN_ROOM:
                    JoinRoomResponse joinRoomResponse = JsonConvert.DeserializeObject<JoinRoomResponse>(jsonStr);
                    return joinRoomResponse;
                case OP_CODES.LEAVE_ROOM:
                    LeaveRoomResponse leaveRoomResponse = JsonConvert.DeserializeObject<LeaveRoomResponse>(jsonStr);
                    return leaveRoomResponse;
                case OP_CODES.HIGH_SCORE:
                    GetStatisticsResponse getStatisticsResponse = JsonConvert.DeserializeObject<GetStatisticsResponse>(jsonStr);
                    return getStatisticsResponse;
            }
            return new Response();
        }

        /*
         * input: op code of the socket, and string of the msg
         * output: none
         * A method that sends a message to the socket
         */
        public void sendMsg(OP_CODES opCode, string msg)
        {
            sock.Send(new byte[1] { (byte)opCode });
            sock.Send(intToBytes(msg.Length));
            sendString(msg);
        }

        /*
         *input: none
         *output: string of data
         *A function that recives from the socket bytes, and parses it to the string of the json
         */
        public string recvJsonStr(int startsAt = 5)
        {
            byte[] buffer = new byte[1024];
            sock.Receive(buffer);
            byte[] jsonBytes = new List<byte>(buffer).GetRange(startsAt, buffer.Length - startsAt).ToArray();
            string jsonStr = System.Text.Encoding.Default.GetString(jsonBytes).Trim('\0');
            return jsonStr;
        }

        public byte getOpCode()
        {
            byte[] res = new byte[1];
            sock.Receive(res);
            return res[0];
        }

        public void cleanBuffer()
        {
            byte[] res = new byte[1024];
            sock.Receive(res);
        }

        public void closeSocket()
        {
            sock.Close();
        }

        /*
         * input: string of msg
         * output: none
         * A method that sends the string through the socket to the server
         */
        public void sendString(string msg)
        {
            byte[] byData = System.Text.Encoding.ASCII.GetBytes(msg);
            sock.Send(byData);
        }

        /*
         * input: integer number
         * output: serilization of the number on 4 bytes
         * A static function that turns an int to array of 4 bytes
         */
        public static byte[] intToBytes(int num)
        {
            byte[] arr = new byte[]{ 0, 0, 0, 0 };
            arr[3] = (byte)((num >> 24) & 0xff);
            arr[2] = (byte)((num >> 16) & 0xff);
            arr[1] = (byte)((num >> 8) & 0xff);
            arr[0] = (byte)(num & 0xff);

            return arr;
        }
        
    }
}
