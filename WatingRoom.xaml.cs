﻿using System.Windows;
using System.Threading;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Diagnostics;
using System;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for WatingRoom.xaml
    /// </summary>
    public partial class WatingRoom : Window
    {
        private bool isFinalExit = true;
        private Thread updateThread;
        public WatingRoom(string title)
        {
            InitializeComponent();

            while(true)
            {
                var jsonMsg = new { };
                GetRoomStateResponse result = (GetRoomStateResponse)MainWindow.commiunicator.handleMsg(jsonMsg, OP_CODES.GET_ROOM_STATE);
                
                //in the rare accident where the parsing has a problem, just try again
                if (result.players == null)
                    continue;

                Title.Text = title;
                TimePerQuestion.Text = result.timePerQuestion.ToString() + "ms";
                Info.Text = "Hi, " + MainWindow.user.getName();
                if (MainWindow.user.isAdmin()) Info.Text += "\r\nYou are an admin!";

                for (int i = 0; i < result.players.Length; i++)
                {
                    Players.Items.Add(result.players[i]);
                }
                break;
            }
            
            updateThread = new Thread(checkForUpdate);
            updateThread.Start();
            
        }

        ~WatingRoom()
        {
            updateThread.Abort();
            this.Close();
        }

        /*
         * input: none
         * output: none
         * A function that the thread runs, it waits for a message from the server
         * and when it gets an answer it updates the information on the screen
         */
        [STAThread]
        private void checkForUpdate()
        {
            while (true)
            {
                switch ((OP_CODES)MainWindow.commiunicator.getOpCode())
                {
                    //wait for incoming updates from the server
                    case OP_CODES.LEAVE_ROOM:
                        MainWindow.commiunicator.cleanBuffer();
                        isFinalExit = false;
                        MessageBox.Show("The admin has left - going to back the menu");
                        Dispatcher.Invoke(() =>
                        {
                            var newForm = new Menu(); //create your new form.
                            newForm.Show(); //show the new form.
                            this.Close();
                        });
                        return;

                    case OP_CODES.GET_ROOM_STATE:
                        string jsonStr = MainWindow.commiunicator.recvJsonStr(4);
                        GetRoomStateResponse result = JsonConvert.DeserializeObject<GetRoomStateResponse>(jsonStr);

                        //updating the information on the screen
                        Dispatcher.Invoke(() =>
                        {
                            TimePerQuestion.Text = result.timePerQuestion.ToString() + "ms";
                            Players.Items.Clear();
                            for (int i = 0; i < result.players.Length; i++)
                            {
                                Players.Items.Add(result.players[i]);
                            }
                        });
                        break;
                }

            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            updateThread.Abort();
            if (isFinalExit)
            {
                (Process.GetCurrentProcess()).Kill();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(MainWindow.user.isAdmin())
            {
                MessageBox.Show("Starting the game...\r\nBut v4 is not ready");
            }
            else
            {
                MessageBox.Show("Wait for the admin to start the game!");
            }
            
        }
    }
}
