﻿namespace TriviaClient
{
    public class User
    {
        private string name;
        private int roomId;
        private bool admin;

        public User(string name)
        {
            this.name = name;
            this.roomId = -1;
            this.admin = false;
        }

        public void setAdmin(bool admin)
        {
            this.admin = admin;
        }

        public bool isAdmin()
        {
            return this.admin;
        }

        public string getName()
        {
            return this.name;
        }

        public void setName(string name)
        {
            this.name = name;
        }
    }
}
