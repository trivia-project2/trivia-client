﻿using System;
using System.Windows;
using System.Threading;
using System.ComponentModel;
using System.Diagnostics;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for JoinRoomPage.xaml
    /// </summary>


    public partial class JoinRoomPage : Window
    {
        static RoomData[] rooms = {};
        static Thread updateThread;
        public JoinRoomPage()
        {
            InitializeComponent();
            updateThread = new Thread(updateThreadFunc);
            updateThread.Start();
        }

        ~JoinRoomPage()
        {
            updateThread.Abort();
        }

        /*
         * input: none
         * output: none
         * A function that the thread runs,
         * it asks for data from the server, updates it on the screen,
         * waits 3 seconds and does it again to always be updated
         */
        public void updateThreadFunc()
        {
            while(true)
            {
                updateRoomData();
                updateScreen();
                Thread.Sleep(3000);
            }
        }

        /*
         * input: none
         * output: none
         * A method that updates the information of the rooms avilable from the server
         */
        public void updateRoomData()
        {
            var jsonMsg = new {};
            GetRoomResponse result = (GetRoomResponse)MainWindow.commiunicator.handleMsg(jsonMsg, OP_CODES.GET_ROOMS);
            rooms = result.result;
        }

        /*
         * input: none
         * output: none
         * A method that takes the instance's room data and updates the screen
         */
        public void updateScreen()
        {     
            this.Dispatcher.Invoke(() =>
            {
                var selected = RoomsList.SelectedItem;
                RoomsList.Items.Clear();
                RoomsList.SelectedItem = selected;
                for (int i = 0; i < rooms.Length; i++)
                {
                    RoomsList.Items.Add(rooms[i].name);
                }
            });

        }

        /*
         * A method that happens everytime the user presses the "Join Room" button
         */
        private void JoinRoom(object sender, RoutedEventArgs e)
        {
            if(RoomsList.SelectedItems.Count == 0)
            {
                MessageBox.Show("Please choose a room!");
            }
            else
            {
                string roomName = RoomsList.SelectedItem.ToString();
                int roomId = getRoomByName(roomName).id;

                var jsonMsg = new {roomId = roomId};
                JoinRoomResponse result = (JoinRoomResponse)MainWindow.commiunicator.handleMsg(jsonMsg, OP_CODES.JOIN_ROOM);
                
                if(Convert.ToBoolean(result.status))
                {
                    var newForm = new WatingRoom(roomName); //create your new form.
                    newForm.Show(); //show the new form.
                    Dispatcher.Invoke(() =>
                    {
                        // Code causing the exception or requires UI thread access
                        this.Close(); //only if you want to close the current form.
                        updateThread.Abort();
                    });
                }
                else
                {
                    MessageBox.Show("Unable to join the room:\r\n" + roomName);
                }
                
            }
            
        }

        private RoomData getRoomByName(string name)
        {
            for(int i = 0; i < rooms.Length; i++)
            {
                if(name == rooms[i].name)
                {
                    return rooms[i];
                }
            }
            return rooms[0];
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            updateThread.Abort();
            //(Process.GetCurrentProcess()).Kill();

        }

        private void ToMenu(object sender, RoutedEventArgs e)
        {
            var newForm = new Menu(); //create your new form.
            newForm.Show(); //show the new form.
            Dispatcher.Invoke(() =>
            {
                // Code causing the exception or requires UI thread access
                this.Close(); //only if you want to close the current form.
            });
        }
    }
}
