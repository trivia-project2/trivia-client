﻿
namespace TriviaClient
{
    public class Response
    {
    }

    class LoginResponse : Response
    {
        public int status;
    };

    class SignupResponse : Response
    {
        public int status;
    };

    class ErrorResponse : Response
    {
        public string message;
    };

    class CreateRoomResponse : Response
    {
        public int status;
    };

    class RoomData
    {
        public int id;
        public string name;
        public int maxPlayers;
        public int currPlayers;
        public int numOfQuestionslnGame;
        public int timePerQuestion;
        public bool isActive;
    }

    class GetRoomResponse : Response
    {
        public int status;
        public RoomData[] result;
    }

    class LeaveRoomResponse : Response
    {
        public int status;
    }

    class GetRoomStateResponse : Response
    {
        public int status;
        public bool hasGameBegun;
        public int timePerQuestion;
        public string[] players;
    }

    class GetStatisticsResponse : Response
    {
        public int status;
        public string[] result;
    }

    class StartGameResponse : Response
    {
        public int status;
    }

    class CloseRoomResponse : Response
    {
        public int status;
    }

    class JoinRoomResponse : Response
    {
        public int status;
    }

}
