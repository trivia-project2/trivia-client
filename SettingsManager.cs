﻿using System.IO;

namespace TriviaClient
{
    public class SettingsManager
    {
        //deafult values
        private string fileSettings = "settings.ini";
        private string host = "127.0.0.1";
        private int port = 8876;

        public SettingsManager()
        {
            if (File.Exists(fileSettings))
            {
                loadFile();
            }
            else
            {
                updateChanges();
            }
        }

        public void updateChanges()
        {
            string[] values = { "host="+host ,
                                "port="+port.ToString()};
            File.WriteAllLines(fileSettings, values);
        }

        public void loadFile()
        {
            string[] settings = File.ReadAllLines(fileSettings);
            for (int i = 0; i < settings.Length; i++)
            {
                string[] paramater = settings[i].Split('=');
                switch (paramater[0])
                {
                    case "host":
                        this.host = paramater[1];
                        break;
                    case "port":
                        this.port = int.Parse(paramater[1]);
                        break;
                }
            }
        }

        public int getPort()
        {
            return this.port;
        }

        public string getHost()
        {
            return this.host;
        }


    }
}
